function [op_ftr]=replace_missing_values(inpftr)
% this function replaces the missing values from a feature with reasonable
% values

op_ftr=inpftr;
nan_ids=find(isnan(inpftr));
% is it numerical or categorical?
% if ~isempty(nan_ids)
%     op_ftr(nan_ids)=-inf;
%     values=unique(op_ftr);
%     values=values(values>-inf);
%     nvs=length(values);
%     if isitcategorical(op_ftr)
%         % categorical
%         for i=1:length(nan_ids)      
%             id=randi([1,nvs]);% select values from the domain randomly
%             op_ftr(nan_ids(i))=values(id);
%         end
%         
%     else % numerical
        op_ftr(nan_ids)=0;
        for i=1:length(nan_ids)
            op_ftr(nan_ids(i))=mean(op_ftr);
        end
%     end
% end

end