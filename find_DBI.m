function DBI=find_DBI(inpdata,cl_centers,clusters,Nc)
% This function meant to calculate Davies-Boulding index for each clustering criteria
%inputs:
%1-inpdata: normalized data
%2-clind: cluster indecis as an array
%3-cl_centers:cluster centroids
%4- clusters isulated as cell arrays
%5- Nc: number of clusters
% @khalid Aljabery
[n,w]=size(inpdata);% number of dimensions
Dbi=0;
Dic=zeros(Nc,1);% distance average for each cluster
%%calculating percluster distance

for i=1:Nc
    tmpd=0;
    if w>1
       x2=cl_centers(i,:);
    else
       x2=cl_centers(i);% this is just in cases when we have single feature
    end
    for j=1:length(clusters{i})
        x1=inpdata(clusters{i}(j),:);
        
        di=find_distance(x1,x2);
        tmpd=tmpd+sqrt(di);
    end
    tmpd=tmpd/length(clusters{i});
    Dic(i)=tmpd;
end
%% calculate the 2nd part of the DB eq

for i=1:Nc
    MaxDij=0;
    for j=1:Nc
        if i~=j
            tmpd=Dic(i)+Dic(j);
            if w>1
               dij=sqrt(sum((cl_centers(i,:)-cl_centers(j,:)).^2));
            else
               dij=sqrt((cl_centers(i)-cl_centers(j)).^2);% this is just in cases when we have single feature
            end
            tmpd=tmpd/dij;
            if tmpd>MaxDij
                MaxDij=tmpd;
            end
        end
    end
    Dbi=Dbi+MaxDij;
end
Dbi=Dbi/Nc;
DBI=Dbi;
end

    
            
        
    