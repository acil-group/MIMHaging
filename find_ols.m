function [outliers,oinds]=find_ols(datain)
cls=unique(datain);
o_ids=[];
oinds=[];
for i=1:length(cls)
    inds=find(datain==cls(i));
    if length(inds)<2
        o_ids=[o_ids,i];
        oinds=[oinds;inds];
    end
end
outliers=o_ids;
end