function labels=implement_kmeans(datasetinp,mode,k)
%this function just to calculate k-means for values of k from 2 to 10
n=size(datasetinp,1);

lbls=zeros(n,k-1);
for i=1:k-1
    if strcmp(mode,'kmeans')
    tmp=kmeans(datasetinp,i+1);
    else
        
        tmp=kmedoids(datasetinp,i+1);
    end
    lbls(:,i)=tmp;
end
labels=lbls;
end