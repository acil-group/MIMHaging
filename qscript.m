% load imagingcell
% load volumescell;
%% Import the data
% [~, ~, datasetcell] = xlsread('S:\khalid\MIMH\correlationremoval\volumes.xlsx','Sheet1','A3:AZ53');
% datasetcell(cellfun(@(x) ~isempty(x) && isnumeric(x) && isnan(x),datasetcell)) = {''};
datasetcell=datasetcell(:,2:end);
ftrs=size(datasetcell,1);
numericcorr=zeros(ftrs,ftrs);
ftrlist1=[];% correlated
ftrlist2=[];% non correlated
for i=1:ftrs
    j=1;
    F=0;
    while j<i
        celldata=datasetcell{j,i};
        if ~isnumeric(celldata)
            starid=strfind(celldata,'*');
            celldata=celldata(1:starid(1)-1);
            numv=str2double(celldata);
            numericcorr(j,i)=numv;
            if numv>0.75
                ftrlist1=[ftrlist1;i];
                F=1;
                break;
            end
        end
        j=j+1;
    end
    if ~F
        ftrlist2=[ftrlist2;i];
    end
        
end
n=length(ftrlist1);
m=length(ftrlist2);
if m>=n
    ftrlists=zeros(m,2);
else
    ftrlists=zeros(n,2);
end
ftrlists(1:n,1)=ftrlist1;
ftrlists(1:m,2)=ftrlist2;
labels={'high correlation > 0.75 ', 'low correlation'};
fname=input('pls, enter the full excel file name: ','s');
xlswrite(fname,labels);
xlswrite(fname,ftrlists,1,'A2');



