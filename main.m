% clc;
% clear all;
inpdset1=xlsread('imaging.xlsx');
inpdset=inpdset1(:,2:end);

parameter=' ';
while ~ strcmp(parameter,'complete')&& ~strcmp(parameter,'ward') && ~strcmp(parameter,'median')
    parameter=input('Pls, enter the linkage tool paramete:[complete, ward, or median]','s');
end

labels=find_HIC(inpdset,0,parameter);
ftrs=size(inpdset,2);
cutinglvls=size(labels,2);
vinds=zerso(3,cutinglvls);
for i=1:cutinglvls
    tmp=labels(:,i);
    [olsid,o_inds]=find_ols(tmp);% get an array that has the outliers cluster indices
    if ~isempty(o_inds)
        inpdset=remove_outliers(inpdset,o_inds);
        labels=find_HIC(inpdset,1,parameter);
        tmp=labels(:,i);
    end
    clsind=unique(tmp);
    Nc=length(clsind);
    clusters=cell(Nc,1);
    centroids=zeros(Nc,ftrs);
    for j=1:Nc
        clusters{j}= find(tmp==clsind(j));
        tmpc=inpdset(clusters{j});
        centroids(j,:)=mean(tmpc);
    end
    DBi=find_DBI(inpdset,centroids,clusters,Nc);
    vinds(1,i)=DBi;% davis boulding index
    SI=find_SI(Nc,clusters,inpdset,tmp);
    vinds(2,i)=SI;% sillhouete index
    CHI=find_CHI(inpdset,clusters,Nc,centroids);
    vinds(3,i)=CHI;
    
end
final=[labels;vinds];
fname=['output_',parameter,'_linkage.xlsx'];
xlswrite(fname,final);

    




