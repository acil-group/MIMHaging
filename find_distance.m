function d=find_distance(x1,x2)
%This function measures the distance between two data samples x1 and x2
x1(isnan(x1))=0;
x2(isnan(x2))=0;
d1=(x1-x2).^2;
d= sum(d1);
end