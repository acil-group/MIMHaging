function [norm_data]=normalize_dset(raw_data)
% this function normalized data set between [0-1] based on min-max
[n,ftrs]=size(raw_data);
norm_data=zeros(n,ftrs);
for i=1:ftrs
    tmp=raw_data(:,i);
    if sum(tmp)>0
    if i>41
        nsids=find(tmp==0);
        if ~isempty(nsids)
            for j=1:length(nsids)
                tmp(nsids(j))=mean(tmp);
            end
        end
    end
    ftrmin=min(tmp);
    ftrmax=max(tmp);
    norm_data(:,i)=(tmp-ftrmin)/(ftrmax-ftrmin);
    else
        disp(['feature: (',num2str(i), ') is an empty feature']);
    end
end

end