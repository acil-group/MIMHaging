function [PCA]=find_PCA(inpmatrix)
%% this function calculates the PCA of any input data matrix
%function designed by Khalid Aljabery direct any quesions to kkatnb@mst.edu
    tmpim=inpmatrix;
    n=size(tmpim,2); % find the number of coluomns in the image
    for j=1:n
        % subtract the mean from each column
        tmpim(:,j)=tmpim(:,j)-mean(tmpim(:,j));
    end
    % calculate the covariance
    covim=cov(tmpim);
    % calculate eigen vectors and eigen values
    [U,S,V]=svd(covim);
    chk=0;
    k=1;
    S_d=sum(diag(S));
    % from this line control the ratio of the variance to be retained
    Th=[];
    while isempty(Th)
        Th=input('pls, Enter the required TH for PCA 0.95 or 0.99: ');
    end
    while chk<Th
        chk=0;
        for j=1:k
            chk=chk+S(j,j);
        end
        chk=chk/S_d;
        k=k+1;
    end
    % calculate the reduced image
    U_reduced=U(:,1:k-1);
    R_PCA=U_reduced'*tmpim';
    PCA=R_PCA';
end