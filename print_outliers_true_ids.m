function [newolsset]=print_outliers_true_ids(olshistory,newols,cutlevel)
if ~isempty(olshistory)
    
    
    for i=1:length(newols)
        offset=0;
        for j=1:length(olshistory)
            if olshistory(j)<=newols(i)
                offset=offset+1;
            end
        end
        newols(i)=newols(i)+offset;
    end
    newolsset=[olshistory;newols];
    
else
    newolsset=newols;
end
  disp(['At cuting level (',num2str(cutlevel),') following outliers detected and removed']);
    newolsset  
    
end
        