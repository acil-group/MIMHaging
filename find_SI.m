function SI=find_SI(Nc,clusters,datainp,labels)
% this function finds the Sillhouette index
%Nc: number of clusters
%clusters: indices of data samples grouped into clusters
%datainp: rawdata
%labels: cluster indices
SI=0;
Sih=silhouette(datainp,labels,'Euclidean');
Sih(isnan(Sih))=0;
for i=1:Nc
%     if sum(find(ols,i))==0
    SI=SI+sum(Sih(clusters{i}))/length(clusters{i});
%     end
end
SI=SI/(Nc);%-length(ols));
end
        
    
