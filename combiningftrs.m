inpdsetraw=imagingcell;
[n,ftrs]=size(inpdsetraw);
n=n-1;
tmp=inpdsetraw(1,1:end);
newdata=zeros(n,ftrs);
pairsid=0;
rhftr=[];
inpdsetraw=inpdsetraw(2:end,:);
% replacing missing values with mean


for i=2:ftrs
    tmpc=zeros(n,1);
    idsm=[];
    for j=1:n
        tmpcell=inpdsetraw{j,i};
        if ~isnumeric(tmpcell)
            tmpc(j)=0;
            idsm=[idsm;j];
        else
            tmpc(j)=tmpcell;
        end
    
    end
    if ~isempty(idsm)
        mn=mean(tmpc);
        tmpc(idsm)=mn;
        inpdsetraw(:,i)=num2cell(tmpc);
    end
    
end
for i=1:length(tmp)
    F=0;
    ind=strfind(tmp{i},'_lh_');
    
    if ~isempty(ind)
        sftr=tmp{i};
        for j=i+1:length(tmp)
            
            indt=strfind(tmp{j},'_rh_');
            tftr=tmp{j};
            if ~isempty(indt) && strcmp(sftr(1:ind),tftr(1:ind)) && strcmp(sftr(ind+2:end),tftr(ind+2:end))
                pairsid=pairsid+1;
                tmp{i}(ind+1)='U';
                for k=1:n
                    tmpstr=inpdsetraw{k,i};
                    tmptrstr=inpdsetraw{k,j};
                    if ~isnumeric(tmpstr)
                        tmpstr=0;
                    end
                    
                    if ~isnumeric(tmptrstr)
                        tmpstr=0;
                    end
                    tmpsum=tmpstr+tmptrstr;
                    newdata(k,i)=tmpsum;
                end
                rhftr=[rhftr,j];
                F=1;
                disp(['pair(',num2str(pairsid),') is: (',num2str(i),',',num2str(j),')']);
                break;
            end
        end
    else
        if ~ismember(i,rhftr)
            for k=1:n
                tmpstr=inpdsetraw{k,i};
                if ~isnumeric(tmpstr)
                    tmpstr=0;
                end
                newdata(k,i)=tmpstr;
                
            end
        end
    end
end
xlswrite('combinedfeatures.xlsx',tmp);
xlswrite('combinedfeatures.xlsx',newdata,1,'A2');



